import pronto
output_file_location='cl.txt'
ont = pronto.Ontology('http://purl.obolibrary.org/obo/cl.owl')

test = ont['CL:0000787']
assert test.name == 'memory B cell'

# This is in dataset D#7166
test = ont['CL:0000842']
assert test.name == 'mononuclear cell'

# This is in dataset D#6611 and D#6612
test = ont['CL:0001024']
assert test.name == 'CD34-positive, CD38-negative hematopoietic stem cell'


output_file = open(output_file_location,"w") 
 
 
for term in ont:
    output_file.write(term.id+"\t"+term.name+"\n")



output_file.close() 

